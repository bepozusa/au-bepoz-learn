﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using Xamarin.Forms;
using Xamarin;

using bepoz_learn;
using bepoz_learn.Shared;
using bepoz_learn.Shared.Colours;

namespace bepoz_learn_ios
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		UIWindow window;

		private LibraryLoader libraryLoader;

		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			window = new UIWindow (UIScreen.MainScreen.Bounds);

			Forms.Init();
			FormsMaps.Init();

			UINavigationBar.Appearance.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0);
			UINavigationBar.Appearance.TintColor = Colour.navigation_bar_colour_top.ToUIColor ();
			UINavigationBar.Appearance.BarTintColor = UIColor.White;
			UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes()
				{
					TextColor = UIColor.Black,
					Font = UIFont.FromName("Arial-BoldMT", 16f)
				});
						
			libraryLoader = new LibraryLoader ();
			libraryLoader.setMp4Path ();
			libraryLoader.checkExistingFiles ();

			window.RootViewController = App.GetMainPage().CreateViewController();
			window.MakeKeyAndVisible ();

			return true;
		}
	}
}

