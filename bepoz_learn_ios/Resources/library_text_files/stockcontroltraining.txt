Overview of Stock Control,1,http://player.vimeo.com/video/98108340,https://bepozlearnmp4.blob.core.windows.net/stockcontroltraining/Stock Control Overview.mp4
Suppliers Overview,1,http://player.vimeo.com/video/98108339,https://bepozlearnmp4.blob.core.windows.net/stockcontroltraining/Overview of Suppliers.mp4
Create and Receive Manual Purchase Orders,2,http://player.vimeo.com/video/98108341,https://bepozlearnmp4.blob.core.windows.net/stockcontroltraining/Creating and Receiving Manual Purchase Orders.mp4
Automatic Purchase Orders,2,http://player.vimeo.com/video/98108515,https://bepozlearnmp4.blob.core.windows.net/stockcontroltraining/Automatic Purchase Orders in Bepoz.mp4
Manual Stock Receipt,2,http://player.vimeo.com/video/98108514,https://bepozlearnmp4.blob.core.windows.net/stockcontroltraining/Manual Stock Receipts in BackOffice.mp4
Stocktake in BackOffice,3,http://player.vimeo.com/video/98108973,https://bepozlearnmp4.blob.core.windows.net/stockcontroltraining/Stocktake in BackOffice.mp4
Stocktaking Using SmartPDE,3,http://player.vimeo.com/video/98108516,https://bepozlearnmp4.blob.core.windows.net/stockcontroltraining/Stocktake using the SmartPDE Devices.mp4
Stock Functions at a Till,4,http://player.vimeo.com/video/98108972,https://bepozlearnmp4.blob.core.windows.net/stockcontroltraining/Stock Functions at the Bepoz Till.mp4
titles
LESSON 1 - OVERVIEW: 20 MINUTES, 2
LESSON 2 - PURCHASING AND RECEIVING: 1 HOUR, 3
LESSON 3 - STOCKTAKE: 40 MINUTES, 2
LESSON 4 - OTHER STOCK FUNCTIONS: 30 MINUTES, 1