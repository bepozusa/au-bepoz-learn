using System;

using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using bepoz_learn_ios.Renderers;

[assembly: Dependency (typeof (BaseUrl_iOS))]

namespace bepoz_learn_ios.Renderers
{
	public interface IBaseUrlIOS { string Get(); }

	public class BaseUrl_iOS : IBaseUrlIOS {
		public string Get () {
			return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); // to load resources in the iOS app itself
		}
	}
}
