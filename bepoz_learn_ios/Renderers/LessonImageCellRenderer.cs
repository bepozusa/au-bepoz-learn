using System.Drawing;

using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

using MonoTouch.UIKit;

using bepoz_learn.Shared;
using bepoz_learn.Shared.Colours;
using bepoz_learn_ios.Renderers;

[assembly: ExportCell (typeof (ImageCell), typeof (LessonImageCellRenderer))]

namespace bepoz_learn_ios.Renderers
{
	public class LessonImageCellRenderer : ImageCellRenderer
    {
		public override UITableViewCell GetCell (Cell item, UITableView tableView)
        {
			var cellView = base.GetCell (item, tableView);

			cellView.BackgroundColor = Colour.menu_background_colour.ToUIColor ();
			cellView.TextLabel.Font = UIFont.FromName("Arial", 14);

			cellView.TextLabel.LineBreakMode = UILineBreakMode.WordWrap;
			cellView.TextLabel.Lines = 2;

            return cellView;
        }
    }
}
