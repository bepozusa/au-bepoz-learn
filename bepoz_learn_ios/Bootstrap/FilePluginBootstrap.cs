using Cirrious.CrossCore.Plugins;

namespace bepoz_learn_ios.Bootstrap
{
    public class FilePluginBootstrap
        : MvxLoaderPluginBootstrapAction<Cirrious.MvvmCross.Plugins.File.PluginLoader, Cirrious.MvvmCross.Plugins.File.Touch.Plugin>
    {
    }
}