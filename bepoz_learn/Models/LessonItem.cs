using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

using bepoz_learn.Download;

namespace bepoz_learn.Models
{
	public class LessonItem : PropertyControl
	{
		public string LessonName;

		private string title;
		public string Title
		{
			get { 
				return title; 
			}
			set
			{
				if (value != title)
				{
					title = value;
					OnPropertyChanged("Title");
				}
			}
		}
			
		private Color color;
		public Color Color
		{
			get { return color; }
			set
			{
				color = value;
				OnPropertyChanged("Color");
			}
		}

		public string Url;
		public string DownloadUrl;

		public int ListIndex;
		public int LessonNumber;
		public int LibraryNumber;
			
		private int downloadProgress;
		public int DownloadProgress
		{
			get { 

				return downloadProgress; 
			}
			set
			{
				if (value != downloadProgress)
				{
					if (DownloadManager.DownloadsShowing)
						Title = LessonName + " - " + DownloadProgress + "%";
					else
						Title = LessonName;

					downloadProgress = value;
					OnPropertyChanged("DownloadProgress");
				}
			}
		}
	
		private bool downloaded;
		public bool Downloaded
		{
			get { return downloaded; }
			set
			{
				downloaded = value;
				OnPropertyChanged("Downloaded");
			}
		}

		private bool downloading;
		public bool Downloading
		{
			get { return downloading; }
			set
			{
				downloading = value;
				OnPropertyChanged("Downloading");
			}
		}

		public LessonItem()
		{
		}

		public LessonItem(int libraryNumber, int listIndex)
		{
			LibraryNumber = libraryNumber;
			ListIndex = listIndex;
		}
	}
}

