using System;
using System.Collections.ObjectModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using PropertyChanged;

namespace bepoz_learn.Models
{
	public class MenuItem
    {
		public string Title { get; set; }
		public int LibraryNumber { get; set; }

		public MenuItem(string title, int libNumber)
		{
			Title = title;
			LibraryNumber = libNumber;
		}
    }
}

