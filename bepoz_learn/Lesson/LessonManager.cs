﻿using System;
using System.IO;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Generic;

using bepoz_learn.Models;
using bepoz_learn;
using bepoz_learn.Colours;

namespace bepoz_learn.Lesson
{
	public static class LessonManager
	{
		public static IList<GroupItem> LessonData, SearchedData, AllVideosData;

		public static string LibraryName;

		public static int LibraryNumber;

		public static LessonItem CurrentLesson;

		public static bool LibraryChanged;

		public static string SearchQuery;

		static LessonManager()
		{
			SearchQuery = "";

			System.Diagnostics.Debug.WriteLine ("Initiating lesson manager.");

			if (LessonData == null)
				LessonData = new ObservableCollection<GroupItem> ();

			createAllVideosLibrary ();
		}
			
		public static void createSearchList()
		{
			System.Diagnostics.Debug.WriteLine ("Creating lesson search data.");

			SearchedData = new ObservableCollection<GroupItem> ();

			GroupItem searchGroup = new GroupItem () { HeaderTitle = "Search Result" };

			foreach (GroupItem groupItem in AllVideosData) 
			{
				foreach(LessonItem lessonItem in groupItem)
				{
					if (lessonItem.Title.ToLower().Contains (SearchQuery.ToLower()))
						searchGroup.Add (lessonItem);
				}
			}

			SearchedData.Add (searchGroup);
		}

		public static void createAllVideosLibrary()
		{
			System.Diagnostics.Debug.WriteLine ("Creating all videos data.");

			AllVideosData = new ObservableCollection<GroupItem> ();

			createList (AllVideosData, 7);
		}

		public static void createLibrary(int libraryNumber)
		{
			LibraryNumber = libraryNumber;

			LessonData = null;
			LessonData = new ObservableCollection<GroupItem> ();

			createList (LessonData, LibraryNumber);
		}

		public static void createList(IList<GroupItem> Data, int libraryNumber)
		{
			System.Diagnostics.Debug.WriteLine ("Creating list for library: " + libraryNumber);

			int entireIndex = 0;

			foreach (TitleItem titleItem in App.Library.getLibraryTitles(libraryNumber)) 
			{
				GroupItem newGroup = new GroupItem () { HeaderTitle = titleItem.Title };

				newGroup.TextColor = Colour.header_text_colour.ToFormsColor();
				newGroup.BackgroundColor = Colour.header_background_colour.ToFormsColor();

				for (int index = entireIndex; index < (titleItem.LessonNumber + entireIndex); index++) {
					LessonItem newItem = App.Library.getLibraryElement (libraryNumber, index);

					newItem.ListIndex = index;

					if (newItem.Downloaded)
						newItem.Color = Colour.lesson_downloaded_text_colour.ToFormsColor();
					else
						newItem.Color = Colour.default_text_colour.ToFormsColor();
												
					// then add corresponding lessons
					newGroup.Add (newItem);
				}

				Data.Add (newGroup);
				entireIndex += titleItem.LessonNumber;
			}
		}
	}
}

