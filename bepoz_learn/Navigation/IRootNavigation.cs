﻿using System;

using Xamarin.Forms;

using bepoz_learn.PageModels;

namespace bepoz_learn.Navigation
{
    public interface IRootNavigation 
    {
        void PushPage(Page page, BasePageModel model);

        void PopPage();
    }
}

