﻿using System;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_learn.Models;
using bepoz_learn.Lesson;

namespace bepoz_learn.PageModels
{
    public class LessonPageModel : BasePageModel
    {
		public LessonItem lessonItem;

		public LessonPageModel (object data)
        {
			lessonItem = LessonManager.CurrentLesson;
        }
    }
}

