﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Windows.Input;

using Xamarin.Forms;

using bepoz_learn.Models;
using bepoz_learn.PageModels;

namespace bepoz_learn.PageModels
{
	public class MenuPageModel : BasePageModel
	{
		public ObservableCollection<MenuItem> MenuItems { get; set; }

		public List<string> LibraryNames = new List<string>(){"Getting Started", "Utilising Bepoz", "Point of Sale", "Stock Control Training", "Upgraded Users", "Whats New?", "All Videos"};

		public MenuPageModel ()
		{
			MenuItems = new ObservableCollection<MenuItem> ();

			int libraryNumber = 1;

			foreach (var libraryName in LibraryNames) {
				MenuItem newMenuItem = new MenuItem (libraryName, libraryNumber);
				MenuItems.Add (newMenuItem);

				libraryNumber++;
			}

			System.Diagnostics.Debug.WriteLine ("Loading menu with " + MenuItems.Count + " items");
		}
	}
}