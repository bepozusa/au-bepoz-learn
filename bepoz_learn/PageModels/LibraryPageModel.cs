﻿using System;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_learn.Models;
using bepoz_learn.Lesson;

namespace bepoz_learn.PageModels
{
	public class LibraryPageModel : BasePageModel
    {
		private IList<GroupItem> lessonData = new List<GroupItem>();

		public IList<GroupItem> LessonData
		{
			get {
				if (SearchQuery.Length > 0)
					lessonData = LessonManager.SearchedData;
				else
					lessonData = LessonManager.LessonData;

				return lessonData;
			}

			set
			{
				lessonData = value;

				RaisePropertyChanged("LessonData");
			}
		}

		public string SearchQuery
		{
			get {
				return LessonManager.SearchQuery; 
			}

			set
			{
				LessonManager.SearchQuery = value;
				LessonManager.createSearchList ();
				RaisePropertyChanged("SearchQuery");
			}
		}
			
		public LibraryPageModel (object data)
        {
        }
    }
}
