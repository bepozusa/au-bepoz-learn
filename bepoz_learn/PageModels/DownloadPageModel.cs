using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using bepoz_learn.PageModels;
using bepoz_learn.Models;
using bepoz_learn.Download;
using bepoz_learn.Lesson;

using Xamarin.Forms;

namespace bepoz_learn.PageModels
{
	public class DownloadPageModel : BasePageModel
	{
		private IList<LessonItem> downloads;

		public string SearchQuery
		{
			get {
				return DownloadManager.SearchQuery; 
			}

			set
			{
				DownloadManager.SearchQuery = value;
				DownloadManager.createSearchList ();

				RaisePropertyChanged("SearchQuery");
			}
		}

		private Color tableColor;
		public Color TableColor
		{
			get { return tableColor; }
			set
			{
				tableColor = value;
				RaisePropertyChanged("Tablecolor");
			}
		}

		private int tableRowHeight;
		public int TableRowHeight
		{
			get { return tableRowHeight; }
			set
			{
				tableRowHeight = value;
				RaisePropertyChanged("TableRowHeight");
			}
		}

		public DownloadPageModel()
		{
			TableColor = Colours.HexColours.library_page_background_colour.ToFormsColor ();
			TableRowHeight = 100;
		}

		public IList<LessonItem> Downloads
		{
			get {
				if (SearchQuery.Length > 0)
					downloads = DownloadManager.SearchedData;
				else
					downloads = DownloadManager.Downloads;

				return downloads;
			}

			set
			{
				downloads = value;
				RaisePropertyChanged("Downloads");
			}
		}
	}
}