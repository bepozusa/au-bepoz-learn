﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_learn.PageModels;
using bepoz_learn.Lesson;
using bepoz_learn.Colours;
using bepoz_learn.Models;
using bepoz_learn.Download;
using bepoz_learn.Controls;

namespace bepoz_learn.Pages
{
	public partial class DownloadPage : ContentPage
	{
		public DownloadPageModel PageModel { get; set; }

		private SearchBar searchBar;

		private bool searchBarAdded = false;

		// Define command for the items in the TableView which will load a webview to load lesson
		private Command LessonCommand;

		public DownloadPage()
		{
			InitializeComponent ();

			NavigationPage.SetHasNavigationBar (this, true);

			searchBar = new SearchBar { Placeholder = "Search Videos", BackgroundColor = Color.White };

			searchBar.SearchButtonPressed += (e, a) =>
			{
				System.Diagnostics.Debug.WriteLine ("Search bar button pressed.");

				PageModel.SearchQuery = "";
				PageModel.SearchQuery = searchBar.Text;

				UpdateTable();
			} ;

			searchBar.TextChanged += (object sender, TextChangedEventArgs e) => {
				PageModel.SearchQuery = "";
				PageModel.SearchQuery = searchBar.Text;

				UpdateTable();
			};

			// set icon for bottom tab
			Icon = "download.png";
			Title = "Current Downloads";

			download_table.Intent = TableIntent.Menu;

			LessonCommand = new Command<LessonItem>(async (LessonItem parameter) =>
				{			
					System.Diagnostics.Debug.WriteLine ("Lesson pressed - " + parameter.Title);

					LessonManager.CurrentLesson = parameter;

					await this.Navigation.PushAsync(BasePageModel.ResolvePageModel<LessonPageModel> (null));
				});
		}

		private void UpdateTable()
		{
			if (!DownloadManager.ListChanged)
				return;
		
			DownloadManager.DownloadSection.Clear ();

			System.Diagnostics.Debug.WriteLine ("Creating download table.");

			// create table sections from lesson titles
			for (int list_index = 0; list_index < PageModel.Downloads.Count; list_index++) {
				LessonImageCell cell = new LessonImageCell {
					ImageSource = ImageSource.FromFile ("video.png"),
					Command = LessonCommand,
					CommandParameter = PageModel.Downloads [list_index]
				};

				// set bindings for cell
				cell.BindingContext = PageModel.Downloads [list_index];
				cell.SetBinding<LessonItem> (ImageCell.TextProperty, vm => vm.Title, BindingMode.OneWay); 
				cell.SetBinding<LessonItem> (ImageCell.TextColorProperty, vm => vm.Color, BindingMode.OneWay); 

				DownloadManager.DownloadSection.Add (cell);
			}

			download_table.Root = DownloadManager.tableRoot;

			DownloadManager.ListChanged = false;
		}

		private void createToolBarItems()
		{
			ToolbarItems.Add(new ToolbarItem("search", "search.png", async () =>
				{
					System.Diagnostics.Debug.WriteLine ("Download search button pressed.");

					if (searchBarAdded)
						download_stack.Children.Remove(searchBar);
					else
						download_stack.Children.Insert(0, searchBar);

					searchBarAdded = !searchBarAdded;

					download_stack.ForceLayout();
				} ));

			ToolbarItems.Add(new ToolbarItem("trash", "trash.png", async () =>
				{
					System.Diagnostics.Debug.WriteLine("Trash pressed.");
					DownloadManager.RemoveCompleteDownloads();
					UpdateTable();
				} ));
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			System.Diagnostics.Debug.WriteLine ("Download page appeared.");

			DownloadManager.DownloadsShowing = true;

			UpdateTable ();

			resetSearch ();

			createToolBarItems ();
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();

			System.Diagnostics.Debug.WriteLine ("Download page disappeared.");

			// remove download percentages from title so they are not displayed via library page
			DownloadManager.resetTitles ();
			DownloadManager.DownloadsShowing = false;
			DownloadManager.ListChanged = true;

			resetSearch ();

			ToolbarItems.Clear ();
		}

		private void resetSearch()
		{
			PageModel.SearchQuery = "";

			// remove search bar before we leave page
			if (searchBarAdded) {
				download_stack.Children.Remove (searchBar);
				searchBarAdded = !searchBarAdded;
				download_stack.ForceLayout ();
			}
		}
	}
}