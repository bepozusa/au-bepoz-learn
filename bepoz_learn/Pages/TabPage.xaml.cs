﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_learn.PageModels;
using bepoz_learn.Lesson;

namespace bepoz_learn.Pages
{    
	public partial class TabPage : TabbedPage
	{
		public TabPageModel PageModel { get; set; }

		public TabPage ()
		{
			InitializeComponent ();

			Title = LessonManager.LibraryName;

			Children[0] = (BasePageModel.ResolvePageModel<LibraryPageModel> (null));
			Children[1] = (BasePageModel.ResolvePageModel<DownloadPageModel> (null));

			Children [1].Unfocus ();
		}
	}
}

