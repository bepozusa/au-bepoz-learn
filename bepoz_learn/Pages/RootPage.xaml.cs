﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_learn.PageModels;
using bepoz_learn.Models;
using bepoz_learn.Lesson;
using bepoz_learn.Navigation;
using bepoz_learn.Download;

namespace bepoz_learn.Pages
{	
	public partial class RootPage : MasterDetailPage, IRootNavigation
	{	
		public RootPage ()
		{
			InitializeComponent ();

			Master = (BasePageModel.ResolvePageModel<MenuPageModel> (null));
			Detail = new NavigationPage (BasePageModel.ResolvePageModel<TabPageModel> (null));

			setCurrentLibrary (new MenuItem("Getting Started", 1));

			Detail = new NavigationPage (BasePageModel.ResolvePageModel<TabPageModel> (null));

			(Master as MenuPage).Menu.ItemSelected += (sender, args) =>
			{
				MenuItem selectedItem = (MenuItem) args.SelectedItem;

				System.Diagnostics.Debug.WriteLine("Library selected - " + selectedItem.Title);

				setCurrentLibrary(selectedItem);

				Detail = new NavigationPage (BasePageModel.ResolvePageModel<TabPageModel> (null));

				IsPresented = false;
			};
		}

		public void setCurrentLibrary(MenuItem menuItem)
		{
			LessonManager.LibraryName = menuItem.Title;
			LessonManager.LibraryChanged = true;
			LessonManager.createLibrary (menuItem.LibraryNumber);
		}

		public void PushPage (Page page, BasePageModel model)
		{
			((NavigationPage)Detail).PushAsync (page);
		}

		public void PopPage ()
		{
			((NavigationPage)Detail).PopAsync ();
		}
	}
}