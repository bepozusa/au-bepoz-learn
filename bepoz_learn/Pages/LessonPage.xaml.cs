﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_learn.Models;
using bepoz_learn.Pages;
using bepoz_learn.Colours;
using bepoz_learn.PageModels;
using bepoz_learn.Lesson;
using bepoz_learn.Download;

namespace bepoz_learn.Pages
{	
	public partial class LessonPage : ContentPage
	{
		public LessonPageModel PageModel { get; set; }

		public ToolbarItem downloadButton;

		public bool UsingMp4;

		public LessonItem CurrentLesson;

		public LessonPage ()
		{
			InitializeComponent ();

			UsingMp4 = LessonManager.CurrentLesson.Downloaded;
			CurrentLesson = LessonManager.CurrentLesson;

			Title = LessonManager.CurrentLesson.Title;

			web_view.Source = LessonManager.CurrentLesson.Url;
			web_view.VerticalOptions = LayoutOptions.FillAndExpand;

			if (!LessonManager.CurrentLesson.Downloaded || LessonManager.CurrentLesson.Downloading) {
				downloadButton = new ToolbarItem ("download", "download.png", async () => {
					System.Diagnostics.Debug.WriteLine ("Download pressed.");
				});

				ToolbarItems.Add (downloadButton);
			}
		}
	}
}

