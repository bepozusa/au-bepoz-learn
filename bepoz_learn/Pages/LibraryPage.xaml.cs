﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_learn.PageModels;
using bepoz_learn.Lesson;
using bepoz_learn.Colours;
using bepoz_learn.Models;
using bepoz_learn.Controls;
using bepoz_learn.Download;

namespace bepoz_learn.Pages
{
	public partial class LibraryPage : ContentPage
	{
		public LibraryPageModel PageModel { get; set; }

		private SearchBar searchBar;

		private bool searchBarAdded = false;

		public LibraryPage()
		{
			InitializeComponent ();

			NavigationPage.SetHasNavigationBar (this, true);

			searchBar = new SearchBar { Placeholder = "Search Videos", BackgroundColor = Color.White };

			searchBar.SearchButtonPressed += (e, a) =>
			{
				System.Diagnostics.Debug.WriteLine ("Search bar button pressed.");

				PageModel.SearchQuery = "";
				PageModel.SearchQuery = searchBar.Text;
			} ;

			searchBar.TextChanged += (object sender, TextChangedEventArgs e) => {
				System.Diagnostics.Debug.WriteLine ("Search text changed.");

				PageModel.SearchQuery = "";
				PageModel.SearchQuery = searchBar.Text;
			};
					
			// set icon for bottom tab
			Icon = "lesson.png";
			Title = LessonManager.LibraryName;

			lesson_list.GroupHeaderTemplate = new DataTemplate(typeof(HeaderViewCell));
			lesson_list.ItemTemplate = new DataTemplate(typeof(LessonViewCell));

			// lesson item when selected navigates to lesson page
			lesson_list.ItemSelected += (sender, e) => {
				LessonManager.CurrentLesson = e.SelectedItem as LessonItem;
				Navigation.PushAsync(BasePageModel.ResolvePageModel<LessonPageModel> (null));
			};
		}

		private void createToolBarItems()
		{
			ToolbarItems.Add(new ToolbarItem("search", "search.png", async () =>
				{
					System.Diagnostics.Debug.WriteLine ("Lesson search button pressed.");

					if (searchBarAdded)
						library_stack.Children.Remove(searchBar);
					else
						library_stack.Children.Insert(0, searchBar);

					searchBarAdded = !searchBarAdded;

					library_stack.ForceLayout();
				} ));
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			System.Diagnostics.Debug.WriteLine ("Library page appeared.");

			resetSearch ();

			createToolBarItems ();
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();

			System.Diagnostics.Debug.WriteLine ("Library page disappeared.");

			resetSearch ();

			ToolbarItems.Clear ();
		}

		private void resetSearch()
		{
			PageModel.SearchQuery = "";

			// remove search bar before we leave page
			if (searchBarAdded) {
				library_stack.Children.Remove (searchBar);
				searchBarAdded = !searchBarAdded;
				library_stack.ForceLayout ();
			}
		}
	}
}