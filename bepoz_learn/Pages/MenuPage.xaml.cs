﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_learn.PageModels;
using bepoz_learn.Colours;
using bepoz_learn;

namespace bepoz_learn.Pages
{	
	public partial class MenuPage : ContentPage
	{	
		public MenuPageModel PageModel { get; set; }

		public ListView Menu { get; set; }

		public MenuPage ()
		{
			InitializeComponent ();

			Title = " "; 
			Icon = "settings.png";

			NavigationPage.SetHasNavigationBar (this, false);

			header_image.Source = Device.OnPlatform(
				iOS: ImageSource.FromFile("logo.png"),
				Android:  ImageSource.FromFile("logo.png"),
				WinPhone: ImageSource.FromFile("logo.png"));

			Menu = menu_list;
		}
	}
}

