﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_learn.Models;
using bepoz_learn.Colours;
using bepoz_learn.Lesson;

namespace bepoz_learn.Download
{
	public static class DownloadManager
	{ 
		public static IList<LessonItem> Downloads, SearchedData;

		public static TableSection DownloadSection;

		public static TableRoot tableRoot;

		public static string SearchQuery;

		public static bool DownloadsShowing, ListChanged;

		static DownloadManager()
		{
			System.Diagnostics.Debug.WriteLine ("Initiating download manager.");

			SearchQuery = "";

			Downloads = new List<LessonItem> ();
			SearchedData = new List<LessonItem> ();

			DownloadSection = new TableSection("Downloads");

			tableRoot = new TableRoot ();
			tableRoot.Add(DownloadSection);

			ListChanged = false;
		}

		public static void createSearchList()
		{
			System.Diagnostics.Debug.WriteLine ("Creating download search data.");

			SearchedData.Clear ();

			foreach (LessonItem lessonItem in Downloads) {
				if (lessonItem.Title.ToLower ().Contains (SearchQuery.ToLower ()))
					SearchedData.Add (lessonItem);
			}

			ListChanged = true;
		}

		public static void AddDownload(LessonItem newLessonItem)
		{
			DownloadManager.ListChanged = true;

			System.Diagnostics.Debug.WriteLine ("Adding download to list " + newLessonItem.Title);

			newLessonItem.Color = Colour.default_text_colour.ToFormsColor();

			// add lesson to group
			Downloads.Add (newLessonItem);

			ListChanged = true;
		}

		public static void resetTitles()
		{
			foreach (LessonItem lessonItem in Downloads)
				lessonItem.Title = lessonItem.LessonName;
		}

		public static void RemoveCompleteDownloads()
		{
			DownloadManager.ListChanged = true;

			System.Diagnostics.Debug.WriteLine ("Removing all completed downloads");

			for (int index = Downloads.Count - 1; index >= 0; index--) {
				if (Downloads [index].Downloaded) {
					RemoveDownload (index);
				}
			}

			ListChanged = true;
		}

		public static void RemoveDownload(int index)
		{
			DownloadManager.ListChanged = true;

			Downloads.RemoveAt (index);

			ListChanged = true;
		}

		public static void RemoveDownload(LessonItem lessonItem)
		{
			DownloadManager.ListChanged = true;

			lessonItem.Title = lessonItem.LessonName;
			Downloads.Remove (lessonItem);

			ListChanged = true;
		}
	}
}