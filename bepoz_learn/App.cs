﻿using System;

using Xamarin.Forms;

using bepoz_learn.Pages;
using bepoz_learn.Navigation;
using bepoz_learn.PageModels;
using bepoz_learn.Lesson;
using bepoz_learn;

namespace bepoz_learn
{
    public class App
    {
		public static LessonLibrary Library;

        public static Page GetMainPage ()
        {	
			var rootPage = new RootPage ();

			TinyIoC.TinyIoCContainer.Current.Register<IRootNavigation> (rootPage);

			return rootPage;
        }
    }
}

