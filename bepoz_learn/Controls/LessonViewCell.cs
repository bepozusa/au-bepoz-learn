﻿using System;

using Xamarin.Forms;

using bepoz_learn.Models;

namespace bepoz_learn.Controls
{
	public class LessonViewCell : ViewCell
	{
		public LessonViewCell()
		{
			this.View = new LessonContentView ();
			Height = 100;
		}
	}
}