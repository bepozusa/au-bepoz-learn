﻿using System;

using Xamarin.Forms;

using bepoz_learn.Models;

namespace bepoz_learn.Controls
{
	public class HeaderViewCell : ViewCell
	{
		public HeaderViewCell()
		{
			this.View = new HeaderContentView ();
			Height = 50;
		}
	}
}

