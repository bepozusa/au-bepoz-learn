﻿using System;
using System.IO;
using System.Collections.Generic;

using Xamarin.Forms;

using bepoz_learn;
using bepoz_learn.Models;

#if __IOS__

using System.IO;
using MonoTouch.Foundation;
using bepoz_learn_ios.Renderers;

#endif

#if __ANDROID__

using Android.Content.Res;
using bepoz_reports_android.Renderers;

#endif

namespace bepoz_learn.Shared
{
	public class LibraryLoader
	{
		// create file list
		private  List<String> files = new List<String>(){"gettingstarted", "utilisingbepoz", "pointofsale", "stockcontroltraining", "upgradedusers", "whatsnew"};
		public bool librariesLoaded;

		#if __ANDROID__

		public AssetManager assets { get; set; }

		public LibraryLoader(AssetManager a)
		{
			assets = a;

			initLibrary ();
		}

		#endif

		public LibraryLoader()
		{
			initLibrary ();
		}

		private void initLibrary()
		{
			Console.WriteLine("Loading complete library.");

			App.Library = new LessonLibrary();

			loadLibraries ();
		}

		private void loadLibraries()
		{
			// load all files
			for (int index = 0; index < files.Count; index++) 
				loadLibrary (index + 1, files[index]);

			// create one list of all videos
			App.Library.createAllVideosLibrary ();
			librariesLoaded = true;
		}

		public void checkExistingFiles()
		{
			// check through all libraries whether each lesson already exists in storage
			for (int indexFiles = 1; indexFiles <= files.Count; indexFiles++) {
				for (int indexLessons = 0; indexLessons < App.Library.getLibrary(indexFiles).Count; indexLessons++) {
					if (File.Exists (App.Library.mp4Path + "/" + App.Library.getLibraryElement (indexFiles, indexLessons).Title + ".mp4"))
						App.Library.getLibraryElement (indexFiles, indexLessons).Downloaded = true;
					else
						App.Library.getLibraryElement (indexFiles, indexLessons).Downloaded = false;
				}

				Console.WriteLine("Checked existing lessons in library " + App.Library.getLibraryTitle(indexFiles));
			}
		}

		public bool checkLibrariesLoaded()
		{
			return librariesLoaded;
		}

		private void loadLibrary(int libraryNumber, String fileName)
		{
			String[] records;

			#if __IOS__
			// read all text from file into string array
			records = File.ReadAllLines("library_text_files/" + fileName + ".txt");

			Console.WriteLine("Loading Library: " + libraryNumber + " with file name: " + fileName + "via __IOS__");
			#elif __ANDROID__
			// Read the contents of our asset
			string content;

			using (StreamReader sr = new StreamReader (assets.Open ("library_text_files/" + fileName + ".txt")))
			{
			content = sr.ReadToEnd ();
			}

			// read all text from file into string array
			records = content.Split ('\n');

			Console.WriteLine("Loading Library: " + libraryNumber + " with file name: " + fileName + "via __ANDROID__");
			#endif

			// pass string into shared library for list storage
			App.Library.loadLibrary (libraryNumber, records);
		}

		public LessonItem getLibraryElement(int libraryNum, int index)
		{
			return App.Library.getLibraryElement(libraryNum, index);
		}

		public List<LessonItem> getLibrary(int libraryNum)
		{
			return App.Library.getLibrary(libraryNum);
		}

		public List<TitleItem> getLibraryTitles(int libraryNum)
		{
			return App.Library.getLibraryTitles(libraryNum);
		}

		public TitleItem getLibraryTitleElement(int libraryNum, int index)
		{
			return App.Library.getLibraryTitleElement(libraryNum, index);
		}

		public String getLibraryTitle(int libraryNum)
		{
			return App.Library.getLibraryTitle(libraryNum);
		}

		public String getMp4Path()
		{
			return App.Library.mp4Path;
		}

		public void setMp4Path()
		{
			#if __IOS__
			// iOS bug means we're using a custom renderer for now, Android and WP need to implement IBaseUrl
			App.Library.mp4Path = DependencyService.Get<IBaseUrlIOS> ().Get ();
			#endif

			#if __ANDROID__
			// iOS bug means we're using a custom renderer for now, Android and WP need to implement IBaseUrl
			App.Library.mp4Path = DependencyService.Get<IBaseUrlAndroid> ().Get ();
			#endif
		}
	}
}

