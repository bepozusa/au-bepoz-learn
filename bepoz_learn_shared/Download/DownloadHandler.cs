using System;
using System.Net;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;

using Xamarin.Forms;

using bepoz_learn.Download;
using bepoz_learn.Lesson;
using bepoz_learn.Models;
using bepoz_learn.Colours;

namespace bepoz_learn.Shared.Download
{
	public class DownloadHandler
	{
		public ToolbarItem DownloadButton;
		private IList<ToolbarItem> toolbarItems;

		public DownloadHandler(ToolbarItem button, IList<ToolbarItem> items)
		{
			DownloadButton = button;
			toolbarItems = items;
		}

		// asynchronous method for downloading lesson .mp4 file
		public async void CreateDownloadHandler (LessonItem newLessonItem)
		{
			string LessonName = newLessonItem.Title;

			System.Diagnostics.Debug.WriteLine("Download pressed.");

			if (newLessonItem.Downloading || newLessonItem.Downloaded){
				System.Diagnostics.Debug.WriteLine("Already downloading lesson '" + newLessonItem.Title + "'");

				return;
			}
	
			DownloadManager.AddDownload (newLessonItem);

			System.Diagnostics.Debug.WriteLine("Starting download for lesson '" + newLessonItem.Title + "'");

			newLessonItem.Downloading = true;

			// create new progress reporter to update download percentage of lesson
			Progress<DownloadBytesProgressItem> progressReporter = new Progress<DownloadBytesProgressItem>();
			progressReporter.ProgressChanged += (s, args) => newLessonItem.DownloadProgress = (int)(args.PercentComplete * 100);

			// start new download task
			Task<int> downloadTask = DownloadHelper.CreateDownloadTask(newLessonItem.DownloadUrl, progressReporter, newLessonItem.Title);

			int bytesDownloaded = await downloadTask;

			// set download status for lesson, if download did not fail, download has complete
			if (bytesDownloaded > 0) 
			{
				System.Diagnostics.Debug.WriteLine("Downloaded complete - {0} bytes.", bytesDownloaded);

				SetDownloadComplete (newLessonItem);
			} 
			else 
			{
				toolbarItems.Add (DownloadButton);

				System.Diagnostics.Debug.WriteLine("Download failed.");

				DownloadManager.RemoveDownload(newLessonItem);
			}

			newLessonItem.Downloading = false;
		}


		private static void SetDownloadComplete(LessonItem lessonItem)
		{
			lessonItem.Title = lessonItem.LessonName;
			lessonItem.Color = Colour.lesson_downloaded_text_colour.ToFormsColor();
			lessonItem.Downloaded = true;

			System.Diagnostics.Debug.WriteLine (lessonItem.Title + " complete.");
		}
	}
}

