using System;
using System.Net;
using System.Threading.Tasks;
using System.IO;

using bepoz_learn.Models;

namespace bepoz_learn.Shared.Download
{
	public static class DownloadHelper
	{
		public static readonly int BufferSize = 4096;
		private const int updateInterval = 50;

		public static async Task<int> CreateDownloadTask(string urlToDownload, IProgress<DownloadBytesProgressItem> progessReporter, string outputFileName)
		{
			int receivedBytes = 0;
			int receivedBytesUpdate = 0;
			int totalBytes = 0;
			int timeout = 10000;

			bool downloadInterrupted = false;

			WebClient client = new WebClient();

			// Create the streams.
			MemoryStream outStream = new MemoryStream();

			try {
				using (var stream = await client.OpenReadTaskAsync(urlToDownload))
				{
					byte[] buffer = new byte[BufferSize];
					totalBytes = Int32.Parse(client.ResponseHeaders[HttpResponseHeader.ContentLength]);

					int bytesRead = 0;

					for (;;)
					{
						var task = stream.ReadAsync(buffer, 0, buffer.Length);

						if (await Task.WhenAny(task, Task.Delay(timeout)) == task) {
							bytesRead = task.Result; 

							// write these bytes to outstream
							await outStream.WriteAsync(buffer, 0, bytesRead);

							if (bytesRead == 0) {
								System.Diagnostics.Debug.WriteLine("Download not interrupted and finished.");

								await Task.Yield ();

								break;
							} else {
								if (receivedBytesUpdate % bytesRead > updateInterval) {
									receivedBytesUpdate = 0;
								}
							}

							receivedBytes += bytesRead;
							receivedBytesUpdate += bytesRead;

							if (progessReporter != null)
							{
								DownloadBytesProgressItem args = new DownloadBytesProgressItem(urlToDownload, receivedBytes, totalBytes);
								progessReporter.Report(args);
							}

						} else { 
							// if task reaches time delay we break the loop
							System.Diagnostics.Debug.WriteLine("Download interruption");
							downloadInterrupted = true;

							break;
						}
					}
				}
					
				if (!downloadInterrupted) {
					System.Diagnostics.Debug.WriteLine("Outstream length: {0}", outStream.Length.ToString());

					// write outstream to mp4 file and save locally
					using (var fileStream = new FileStream(App.Library.mp4Path + "/" + outputFileName + ".mp4", FileMode.CreateNew, FileAccess.ReadWrite))
					{
						outStream.Position = 0;
						outStream.CopyTo(fileStream);

						System.Diagnostics.Debug.WriteLine("File saved to " + App.Library.mp4Path + "/" + outputFileName + ".mp4");
					}
				}

				else {
					return 0;
				}
			}

			catch (Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("Failed to download Mp4 - " + exception.Message);
				return 0;
			}

			return receivedBytes;
		}
	}
}

