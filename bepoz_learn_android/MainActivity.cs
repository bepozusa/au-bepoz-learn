﻿using System;
using System.IO;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using bepoz_learn.Shared;
using bepoz_learn.Pages;

namespace bepoz_learn_android
{
	[Activity(Label = "bepoz_learn", MainLauncher = true, ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
	public class MainActivity : AndroidActivity
	{
		private LibraryLoader libraryLoader;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			Xamarin.Forms.Forms.Init(this, bundle);
			Xamarin.FormsMaps.Init(this, bundle);

			ActionBar.NavigationMode = ActionBarNavigationMode.Standard;

			ActionBar.SetDisplayShowTitleEnabled (false);
			ActionBar.SetDisplayShowHomeEnabled (false); 
			ActionBar.SetDisplayShowCustomEnabled (true);

			ActionBar.SetCustomView (Resource.Menu.lesson_page_menu);

			libraryLoader = new LibraryLoader (Assets);
						
			SetPage (new RootPage());
		}
	}
}