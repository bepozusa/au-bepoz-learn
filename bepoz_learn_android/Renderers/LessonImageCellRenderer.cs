﻿using System;

using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;

using Android.Widget;
using Android.Graphics.Drawables.Shapes;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.App;

using Color = Xamarin.Forms.Color;
using View = global::Android.Views.View;
using ViewGroup = global::Android.Views.ViewGroup;
using Context = global::Android.Content.Context;
using ListView = global::Android.Widget.ListView;

using bepoz_reports.Controls;
using bepoz_reports_android.Renderers;
using bepoz_reports.Shared.Colours;

[assembly: ExportCell (typeof (LessonImageCell), typeof (LessonImageCellRenderer))]

namespace bepoz_reports_android.Renderers
{
	public class LessonImageCellRenderer : ImageCellRenderer
    {
        protected override View GetCellCore (Cell item, View convertView, ViewGroup parent, Context context)
        {
            var cell = (LinearLayout)base.GetCellCore (item, convertView, parent, context);

            cell.SetPadding(20, 10, 0, 10);
            cell.DividerPadding = 50;

            var div = new ShapeDrawable();
            div.SetIntrinsicHeight(1);
			div.Paint.Set(new Paint { Color = Colour.menu_table_view_seperator_colour.ToAndroidColor() });

            if (parent is ListView) {
                ((ListView)parent).Divider = div;
                ((ListView)parent).DividerHeight = 1;
            }

			cell.RootView.SetBackgroundColor(Colour.lesson_page_background_colour.ToAndroidColor());

			var label = (TextView)((LinearLayout)cell.GetChildAt(1)).GetChildAt(0);
			label.SetTextColor(Colour.default_text_colour.ToAndroidColor());
			label.TextSize = Font.SystemFontOfSize(NamedSize.Large).ToScaledPixel();

			Typeface arialTypeFace = Typeface.CreateFromAsset(context.Assets, "fonts/Arial.ttf");

			label.SetTypeface (arialTypeFace, TypefaceStyle.Normal);

            return cell;
        }
    }
}

