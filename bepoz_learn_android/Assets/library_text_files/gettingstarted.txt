Overview of the Bepoz System,1,http://player.vimeo.com/video/97996771,http://bepozlearning.blob.core.windows.net/videos/BEpoz Overview.mp4
Logging into BackOffice,1,http://player.vimeo.com/video/97996770,http://bepozlearning.blob.core.windows.net/videos/BackOfficeLoggingIn.mp4
Overview of Groups and Operators,2,http://player.vimeo.com/video/97996797,http://bepozlearning.blob.core.windows.net/videos/Operator and Groups Overview.mp4
Create a New Operator,2,http://player.vimeo.com/video/97996772,http://bepozlearning.blob.core.windows.net/videos/Operator Create.mp4
Product Overview,2,http://player.vimeo.com/video/98003791,http://bepozlearning.blob.core.windows.net/videos/Product Overview.mp4
Create a New Product,2,http://player.vimeo.com/video/97996798,http://bepozlearning.blob.core.windows.net/videos/Product Create.mp4
Change a Price using the Product List,2,http://player.vimeo.com/video/97996773,http://bepozlearning.blob.core.windows.net/videos/Price Change via Product List.mp4
Product Edit Screen Part 1: Main Setups,3,http://player.vimeo.com/video/97996800,http://bepozlearning.blob.core.windows.net/videos/Product Screen Part1.mp4
Product Edit Screen Part 2: Size & Store Setup,3,http://player.vimeo.com/video/97996836,http://bepozlearning.blob.core.windows.net/videos/Product Screen Part2.mp4
Product Edit Screen Part 3: Product Mode & Flags,3,http://player.vimeo.com/video/97996837,http://bepozlearning.blob.core.windows.net/videos/Product Screen Part3.mp4
Change a Price using the Product Edit Screen,3,http://player.vimeo.com/video/97996799,http://bepozlearning.blob.core.windows.net/videos/Price Change via Product Edit .mp4
Overview of Keymaps and Keysets,4,http://player.vimeo.com/video/97997310,http://bepozlearning.blob.core.windows.net/videos/Keymap Overview.mp4
Add a Product or Group to a Product KeyMap,5,http://player.vimeo.com/video/97997311,http://bepozlearning.blob.core.windows.net/videos/Add Product to KeyMap.mp4
Add a Product Keylist to a Product KeyMap,5,http://player.vimeo.com/video/97996838,http://bepozlearning.blob.core.windows.net/videos/Add Keylist to KeyMap.mp4
Overview of Accounts,6,http://player.vimeo.com/video/97997352,http://bepozlearning.blob.core.windows.net/videos/Accounts Overview.mp4
Create a New Account,6,http://player.vimeo.com/video/97997351,http://bepozlearning.blob.core.windows.net/videos/Account Create.mp4
Account Edit Screen Part1: Basic Setups and Address,6,http://player.vimeo.com/video/97997312,http://bepozlearning.blob.core.windows.net/videos/Account Screen Part1.mp4
Account Edit Screen Part2: Main Setups Tab,6,http://player.vimeo.com/video/97997313,http://bepozlearning.blob.core.windows.net/videos/Account Screen Part2.mp4
Overview of Reporting Concepts,7,http://player.vimeo.com/video/97997350,http://bepozlearning.blob.core.windows.net/videos/Reporting Overview.mp4
Till Balance Explained,7,http://player.vimeo.com/video/97997380,http://bepozlearning.blob.core.windows.net/videos/Till Balance.mp4
Period Summary Report,7,http://player.vimeo.com/video/97997381,http://bepozlearning.blob.core.windows.net/videos/Period Summary Report.mp4
titles
Lesson 1 - Introduction: 15 Minutes, 2
Lesson 2 - Basic Maintenance: 1 Hour, 5
Lesson 3 - Advanced Product Maintenance: 1 Hour, 4
Lesson 4 - Keymaps/Keysets: 30 Minutes, 1
Lesson 5 - Advanced Keymaps: 30 Minutes, 2
Lesson 6 - Accounts: 1 Hour, 4
Lesson 7 - Reporting: 30 Minutes, 3